if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

//Install express server
const express = require('express');
const path = require('path');
const compression = require('compression');

const app = express();

function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if ((!req.secure && req.get('x-forwarded-proto') !== 'https') && process.env.NODE_ENV !== "development") {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}

app.use(requireHTTPS);
app.use(compression());
app.set('view cache', true);

// Serve only the static files form the dist directory
app.use(express.static('./dist/mercial-admin'));

app.get('/*', function(req,res) {
  res.sendFile(path.join(__dirname,'/dist/mercial-admin/index.html'));
});

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 8080);
