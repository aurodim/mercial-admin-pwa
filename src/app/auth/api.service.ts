import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import { MzToastService } from 'ngx-materialize';

@Injectable()
export class APIService {
  public api: string;

  constructor(private http: HttpClient, private authService: AuthService, private toastService: MzToastService) {
    this.api = environment.endpoint;
  }

  AUTH() {
    return this.authService.getAUTH();
  }

  // GET
  dashboardData() {
    return new Promise((resolve, reject) => {
      this.http.get(this.api + '/api/admin/stats/home', { params : { auth : this.authService.getAUTH()}, responseType : 'json' })
      .subscribe((response: any) => resolve(response), (error) => {
        this.serverError(error);
        reject(error);
      });
    });
  }

  videosData() {
    return new Promise((resolve, reject) => {
      this.http.get(this.api + '/api/admin/stats/videos', { params : { auth : this.authService.getAUTH() }, responseType : 'json' })
      .subscribe((response: any) => resolve(response), (error) => {
        this.serverError(error);
        reject(error);
      });
    });
  }

  offersData() {
    return new Promise((resolve, reject) => {
      this.http.get(this.api + '/api/admin/stats/offers', { params : { auth : this.authService.getAUTH() }, responseType : 'json' })
      .subscribe((response: any) => resolve(response), (error) => {
        this.serverError(error);
        reject(error);
      });
    });
  }

  reportsData() {
    return new Promise((resolve, reject) => {
      this.http.get(this.api + '/api/admin/stats/reports', { params : { auth : this.authService.getAUTH() }, responseType : 'json' })
      .subscribe((response: any) => resolve(response), (error) => {
        this.serverError(error);
        reject(error);
      });
    });
  }

  // POST
  initiateLogin(credentials: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(credentials);
      this.http.post(this.api + '/api/admin/actions/login/initiate', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  login(credentials: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(credentials);
      this.http.post(this.api + '/api/admin/actions/login', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  rejectOffer(data: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(data);
      this.http.post(this.api + '/api/admin/actions/offer/reject', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  rejectVideo(data: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(data);
      this.http.post(this.api + '/api/admin/actions/video/reject', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  approveVideo(data: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(data);
      this.http.post(this.api + '/api/admin/actions/video/approve', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  approveOffer(data: object) {
    return new Promise((resolve, reject) => {
      const headers = new HttpHeaders({'Content-Type' : 'application/json'});
      const body = JSON.stringify(data);
      this.http.post(this.api + '/api/admin/actions/offer/approve', body, { headers }).subscribe(
        (response: any) => resolve(response), (error) => {
          this.serverError(error);
          reject(error);
        });
    });
  }

  serverError(error?: { status: any; }) {
    this.toastService.show(`Server Error - ${error.status}`, 3000, 'red');
  }
}
