import { Injectable } from '@angular/core';
import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService {
  private aT: string = null;
  private fullname: string = null;
  private authenticated = false;

  constructor() {
    this.fullname = sessionStorage.getItem('_ax_fn');
    this.aT = sessionStorage.getItem('_ax_at');

    if (this.fullname != null && this.aT != null) {
      this.setAuthenticated(true);
    }
  }

  // SETTERS
  public authenticate(body: any, cb: () => void) {
    this.aT = body._tk;
    this.fullname = body._fn;
    sessionStorage.setItem('_ax_fn', this.fullname);
    sessionStorage.setItem('_ax_at', this.aT);
    this.setAuthenticated(true);
    cb();
  }

  public logoutUser() {
    return new Promise((resolve) => {
      this.fullname = null;
      this.aT = null;
      sessionStorage.removeItem('_ax_fn');
      sessionStorage.removeItem('_ax_at');
      this.setAuthenticated(false);
      resolve();
    });
  }

  public setAuthenticated(authenticated: boolean) {
    this.authenticated = authenticated;
  }

  // GETTERS
  public getAUTH() {
    return this.aT;
  }

  public getSocket() {
    const decodedToken = jwt_decode(this.aT);
    return decodedToken._s;
  }

  public isAuthenticated() {
    return this.authenticated;
  }

  public getFullname() {
    return this.fullname;
  }
}
