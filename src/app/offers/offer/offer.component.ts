import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { APIService } from 'src/app/auth/api.service';
import { MzToastService } from 'ngx-materialize';
import { DomSanitizer } from '@angular/platform-browser';
import { Offer } from './offer.model';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styles: [`
    .tags {
      z-index: 100000;
    }
  `]
})
export class OfferComponent implements OnInit {
  @Input() offer: Offer;
  width: number;

  constructor(private api: APIService, private authService: AuthService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.width = window.innerWidth * 0.60;
  }

  Sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

 // onReportSubmit() {
 //   if (this.reportReason === '') {
 //     return;
 //   }
 //
 //   const report = {
 //     auth : this.authService.getAUTH(),
 //     id : this.mercial.id,
 //     type : 'mercial',
 //     reason : this.reportReason,
 //     expand : this.reportExpand
 //   };
 //
 //   // this.api.report(report, (response: any) => {
 //   //     const res = response._body;
 //   //     if (res._m === '_invalidreason') {
 //   //       this.toastService.show('Invalid reason', 3000, 'red', () => {});
 //   //       return;
 //   //     }
 //   //
 //   //     if (res._m === '_invalidid') {
 //   //       this.toastService.show('Video or Offer has been deleted and is no longer available', 3000, 'red', () => {});
 //   //       return;
 //   //     }
 //   //
 //   //     this.toastService.show('Report submitted', 3000, 'green', () => {});
 //   // });
 //  }
 //
 //  setupInfo() {
 //   const arr = {};
 //   if (this.mercial.more) {
 //       this.mercial.more.forEach(more => {
 //        let formatted: any = '';
 //        let after = [];
 //        let moreCopy = more;
 //        if (moreCopy.includes('://')) {
 //          moreCopy = moreCopy.substring(moreCopy.indexOf('://') + 3);
 //          after = moreCopy.split('/');
 //          after  = after.map(a => a.toUpperCase());
 //        }
 //        formatted = moreCopy.split('/')[0].split('.');
 //        if (formatted.length === 2) {
 //          formatted = formatted[0].substring(0, 1).toUpperCase() + formatted[0].substring(1);
 //        } else {
 //          formatted = formatted[1].substring(0, 1).toUpperCase() + formatted[1].substring(1);
 //        }
 //        if (after.length > 1) {
 //          formatted += ' - ' + after.splice(1).join(' ');
 //        }
 //        arr[formatted] = more;
 //      });
 //    }
 //    return arr;
 //  }

  cleanMore(url: string) {
    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
      url = 'http://' + url;
    }
    return url;
  }
}
