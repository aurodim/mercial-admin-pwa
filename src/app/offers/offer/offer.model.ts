export class Offer {
  id: string;
  franchise: any;
  name: string;
  description?: string;
  image: string;
  locations: any[];
  website: string;
  merchips: number;
  refund: string;
  savings: string;

  constructor(id: string, franchise: any,  name: string, image: string, locations: Array<{formatted: string, lat: number, lng: number}>,
              website: string, merchips: number, savings: string, refund: string,
              description?: string) {
    this.id = id;
    this.franchise = franchise;
    this.name = name;
    this.description = description;
    this.image = image;
    this.locations = locations;
    this.website = website;
    this.merchips = merchips;
    this.refund = refund;
    this.savings = savings;
    console.log(this.image);
  }

  public Cloudinarize(link: string, width: any, extras?: string, isVideo = false, ending = '') {
    const toReturn = link.replace('/upload/', `/upload/w_${width}${extras ? ',' + extras : ''}/`).split('.');
    if (!isVideo) {
      return toReturn.join('.');
    }
    toReturn.pop();
    return toReturn.join('.') + ending;
  }

  public getLocations() {
    return this.locations.slice().map(location => location.formatted).join(', ');
  }
}
