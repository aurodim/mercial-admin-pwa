import { Component, OnDestroy, AfterViewInit } from '@angular/core';
import { Offer } from './offer/offer.model';
import { OfferService } from './offers.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MzToastService } from 'ngx-materialize';
import { APIService } from '../auth/api.service';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss']
})
export class OffersComponent implements AfterViewInit, OnDestroy {
  offers: Offer[] = [];
  keepLoading = true;
  subscriptions = [];
  reportReason = '';
  reportExpand = '';
  reasons = [
    { value : 'unexistent', formatted : 'Inexistent / The business owner is not a member' },
    { value : 'fake', formatted : 'Fake / Business owner scanned my code but did not give me the offer' },
  ];
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: () => { // Callback for Modal open. Modal and trigger parameters available.

    },
    complete: () => { } // Callback for Modal close
  };

  constructor(private offerService: OfferService, private authService: AuthService, router: Router, private toastService: MzToastService,
              private api: APIService) {}

  ngAfterViewInit() {
    if (!this.authService.getAUTH()) {
      return;
    }
    this.offerService.requestFeed(() => {
      this.offers = [];
      this.offers = this.offerService.getFeed();
    }, () => {
      this.toastService.show('Please Refresh', 3000, 'red');
    });
  }

  onRefresh() {
    this.offers = [];
    this.offerService.clearFeed();
    this.offerService.requestFeed(() => {
      this.offers = [];
      this.offers = this.offerService.getFeed();
    }, () => {
      this.toastService.show('Please Refresh', 3000, 'red');
    });
    this.keepLoading = true;
  }

    onReportSubmit() {
      if (this.reportReason === '') {
        return;
      }

      const report = {
        auth : this.authService.getAUTH(),
        id : this.offers[0].id,
        reason : this.reportReason,
        expand : this.reportExpand
      };

      this.api.rejectOffer(report).then(() => {
        this.offers.shift();
        this.reportReason = '';
        this.reportExpand = '';
        this.toastService.show('Offer moderated successfully.', 3000, 'gray');
      }).catch(() => {
        this.toastService.show('Error Occurred. Try Again', 3000, 'red');
      });
    }

    onApprove() {
      this.api.approveOffer({auth: this.authService.getAUTH(), id: this.offers[0].id}).then(() => {
        this.offers.shift();
        this.reportReason = '';
        this.reportExpand = '';
        this.toastService.show('Offer moderated successfully.', 3000, 'gray');
      }).catch(() => {
        this.toastService.show('Error Occurred. Try Again', 3000, 'red');
      });
    }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: { unsubscribe: () => void; }) => {
      subscription.unsubscribe();
    });
  }
}
