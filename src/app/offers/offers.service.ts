import { Injectable } from '@angular/core';
import { APIService } from '../auth/api.service';
import { Offer } from './offer/offer.model';

@Injectable()
export class OfferService {
  private feed: Offer[] = [];

  constructor(private api: APIService) { }

  requestFeed(callback: () => void, onError?: () => void) {
    this.api.offersData().then((response: any) => {
      this.feed = [];
      const res = response._body._d;
      for (const offer of res.o) {
        this.feed.push(new Offer(offer.id, offer.franchise, offer.name, offer.image, offer.locations, offer.website,
          offer.merchips, offer.savings, offer.refund, offer.description));
      }
      callback();
    }).catch(onError);
  }

  getPublisher(publisher: string) {
    if (!publisher) {
      return null;
    }

    if (publisher.replace(/ /gi, '') === '') {
      return null;
    } else {
      return publisher;
    }
  }

  clearFeed() {
    this.feed = [];
  }

  getFeed() {
    return this.feed.slice();
  }

  getFeedItem(id: string) {
    return this.feed.find(ad => ad.id === id);
  }

  setFeed(feed: Offer[]) {
    this.feed = feed;
  }
}
