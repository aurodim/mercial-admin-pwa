import { Component, OnInit } from '@angular/core';
import { APIService } from '../auth/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  offersPosted: number;
  offersRedeemed: number;
  videosPosted: number;
  loaded = false;

  constructor(private api: APIService) { }

  ngOnInit() {
    this.api.dashboardData().then((res: any) => {
      console.log(res);
      const data = res._body._d;
      this.offersPosted = data.o;
      this.offersRedeemed = data.r;
      this.videosPosted = data.m;
      this.loaded = true;
    });
  }

}
