import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { VideosComponent } from './videos/videos.component';
import { OffersComponent } from './offers/offers.component';
import { MessagesComponent } from './messages/messages.component';
import { SettingsComponent } from './settings/settings.component';
import { APIService } from './auth/api.service';
import { AuthService } from './auth/auth.service';
import { MzToastModule, MzSpinnerModule, MzDatepickerModule, MzTooltipModule,
  MzProgressModule, MzCheckboxModule, MzRadioButtonModule, MzCollectionModule, MzInputModule,
  MzSelectModule, MzButtonModule, MzModalModule, MzCardModule, MzCollapsibleModule } from 'ngx-materialize';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './auth/auth.guard';
import { VideoService } from './videos/videos.service';
import { VideoComponent } from './videos/video/video.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { OfferService } from './offers/offers.service';
import { OfferComponent } from './offers/offer/offer.component';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import { Cloudinary } from 'cloudinary-core';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    VideosComponent,
    VideoComponent,
    OffersComponent,
    OfferComponent,
    MessagesComponent,
    SettingsComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    MzToastModule,
    MzModalModule,
    MzButtonModule,
    MzSelectModule,
    MzInputModule,
    MzToastModule,
    MzCollectionModule,
    MzRadioButtonModule,
    MzCheckboxModule,
    MzProgressModule,
    MzTooltipModule,
    MzDatepickerModule,
    MzCardModule,
    MzSpinnerModule,
    MzCollapsibleModule,
    FormsModule,
    InfiniteScrollModule,
    HttpClientModule,
    BrowserModule,
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name: 'aurodim' } as CloudinaryConfiguration),
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AuthService, APIService, AuthGuard, VideoService, OfferService],
  bootstrap: [AppComponent]
})
export class AppModule { }
