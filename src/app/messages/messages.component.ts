import { Component, OnInit } from '@angular/core';
import { APIService } from '../auth/api.service';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {
  messages: any[];
  reasons = {
      disturbing : 'Disturbing content',
      violent : 'Violent or repulsive content',
      hateful : 'Hateful or abusive content',
      terrorism : 'Promotes terrorism',
      infringement : 'Infringes my rights',
      restriction : 'Violates age restriction',
      unexistent : 'Inexistent - The business owner is not a member',
      fake: 'Fake - Business owner scanned my code but did not give me the offer'
  };

  constructor(private api: APIService, private toastService: MzToastService) { }

  ngOnInit() {
    this.api.reportsData().then((res: any) => {
      const data = res._body._d;
      this.messages = data.r;
      console.log(this.messages);
      console.log(res);
    }).catch(() => {
      this.toastService.show('Please Refresh', 3000, 'red');
    });
  }

}
