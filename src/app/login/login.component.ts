import { Component } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { APIService } from '../auth/api.service';
import { NgForm } from '@angular/forms';
import { MzToastService } from 'ngx-materialize';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  continued = false;

  constructor(private authService: AuthService, private toastService: MzToastService, private router: Router, private api: APIService) {
    if (authService.isAuthenticated()) {
      this.router.navigate(['/home']);
    }
  }

  onContinue(loginForm: NgForm) {
    this.api.initiateLogin(loginForm.value).then(() => {
      this.continued = true;
    });
  }

  onLogin(loginForm: NgForm) {
    if (loginForm.invalid) {
      return;
    }
    this.api.login(loginForm.value).then((response: any) => {
      loginForm.resetForm();
      if (response._body._m  === '_success_') {
        this.authService.authenticate(response._body, () => {
          this.router.navigate(['home']);
        });
      } else {
        this.toastService.show('Not Recognized', 3000, 'red');
        this.continued = false;
      }
    }).catch(() => {
      this.continued = false;
    });
  }
}
