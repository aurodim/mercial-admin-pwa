import { Injectable } from '@angular/core';
import { APIService } from '../auth/api.service';
import { Video } from './video/video.model';

@Injectable()
export class VideoService {
  private feed: Video[] = [];

  constructor(private api: APIService) { }

  requestFeed(callback: () => void, onError?: () => void) {
    this.api.videosData().then((response: any) => {
      this.feed = [];
      const res = response._body._d;
      for (const video of res.m) {
        this.feed.push(
          new Video(video.id, this.getPublisher(video.publisher),
           video.title, video.thumbnail, video.video, video.size,
            video.merchips, video.duration, video.tags, video.description,
             video.more, video.attributes));
      }
      callback();
    }).catch(onError);
  }

  getPublisher(publisher: string) {
    if (!publisher) {
      return null;
    }

    if (publisher.replace(/ /gi, '') === '') {
      return null;
    } else {
      return publisher;
    }
  }

  clearFeed() {
    this.feed = [];
  }

  getFeed() {
    return this.feed.slice();
  }

  getFeedItem(id: string) {
    return this.feed.find(ad => ad.id === id);
  }

  hasProperty(id: string, property: string) {
    const ad: Video = this.getFeedItem(id);
    return ad.attributes.indexOf(property) !== -1 ? true : false;
  }

  setFeed(feed: Video[]) {
    this.feed = feed;
  }
}
