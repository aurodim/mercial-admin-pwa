import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { MzToastService } from 'ngx-materialize';
import { Video } from './video/video.model';
import { VideoService } from './videos.service';
import { APIService } from '../auth/api.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements AfterViewInit, OnDestroy {
  videos: Video[] = [];
  keepLoading = true;
  subscriptions = [];
  reportReason = '';
  reportExpand = '';
  reasons = [
      { value : 'disturbing', formatted : 'Disturbing content' },
      { value : 'violent', formatted : 'Violent or repulsive content' },
      { value : 'hateful', formatted : 'Hateful or abusive content' },
      { value : 'terrorism', formatted : 'Promotes terrorism' },
      { value : 'infringement', formatted : 'Infringes my rights' },
      { value : 'restriction', formatted : 'Violates age restriction' },
  ];
  public modalOptions: Materialize.ModalOptions = {
    dismissible: false, // Modal can be dismissed by clicking outside of the modal
    opacity: .5, // Opacity of modal background
    inDuration: 300, // Transition in duration
    outDuration: 200, // Transition out duration
    startingTop: '100%', // Starting top style attribute
    endingTop: '10%', // Ending top style attribute
    ready: () => { // Callback for Modal open. Modal and trigger parameters available.

    },
    complete: () => { } // Callback for Modal close
  };

  constructor(private videoService: VideoService, private authService: AuthService, private toastService: MzToastService,
              private api: APIService) {}

  ngAfterViewInit() {
    if (!this.authService.getAUTH()) {
      return;
    }
    this.videoService.requestFeed(() => {
      this.videos = [];
      this.videos = this.videoService.getFeed();
    }, () => {
      this.toastService.show('Please Refresh', 3000, 'red');
    });
  }

  onRefresh() {
    this.videos = [];
    this.videoService.clearFeed();
    this.videoService.requestFeed(() => {
      this.videos = [];
      this.videos = this.videoService.getFeed();
    }, () => {
      this.toastService.show('Please Refresh', 3000, 'red');
    });
    this.keepLoading = true;
  }

  onReportSubmit() {
    if (this.reportReason === '') {
      return;
    }

    const report = {
      auth : this.authService.getAUTH(),
      id : this.videos[0].id,
      reason : this.reportReason,
      expand : this.reportExpand
    };

    this.api.rejectVideo(report).then(() => {
      this.videos.shift();
      this.reportReason = '';
      this.reportExpand = '';
      this.toastService.show('Video moderated successfully.', 3000, 'gray');
    }).catch(() => {
      this.toastService.show('Error Occurred. Try Again', 3000, 'red');
    });
  }

  onApprove() {
    this.api.approveVideo({auth: this.authService.getAUTH(), id: this.videos[0].id}).then(() => {
      this.videos.shift();
      this.reportReason = '';
      this.reportExpand = '';
      this.toastService.show('Video moderated successfully.', 3000, 'gray');
    }).catch(() => {
      this.toastService.show('Error Occurred. Try Again', 3000, 'red');
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription: { unsubscribe: () => void; }) => {
      subscription.unsubscribe();
    });
  }
}
