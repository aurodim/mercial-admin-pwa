import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { APIService } from 'src/app/auth/api.service';
import { MzToastService } from 'ngx-materialize';
import { DomSanitizer } from '@angular/platform-browser';
import { Video } from './video.model';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styles: [`
    .tags {
      z-index: 100000;
    }
  `]
})
export class VideoComponent implements OnInit {
  @Input() mercial: Video;
  width: number;

  constructor(private api: APIService, private authService: AuthService,
              private sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.width = window.innerWidth * 0.60;
  }

  Sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  cleanMore(url: string) {
    if (url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
      url = 'http://' + url;
    }
    return url;
  }
}
